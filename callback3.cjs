const fs = require('fs');

function getCardInfo(listID, callback) {

    if (arguments.length < 2 || listID === undefined || typeof listID != 'string' || typeof callback != 'function') {
        callback(new Error('Error : Invalid List Id.'))
    }

    setTimeout(() => {
        fs.readFile('./cards.json', 'utf-8', (err, cardsData) => {
            if (err) {
                callback(err);
            }
            else {
                const card = JSON.parse(cardsData)[listID];
                if (card !== undefined) {
                    callback(null, card);
                }
            }
        });
    }, 2000);
}

module.exports = getCardInfo;