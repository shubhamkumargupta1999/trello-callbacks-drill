const getBoardInfo = require('./callback1.cjs');
const getListInfo = require('./callback2.cjs');
const getCardInfo = require('./callback3.cjs');

function doAgain(value1, arr, callback) {

    if (arguments.length < 3 || typeof value1 != 'string' || !Array.isArray(arr) || typeof callback != 'function') {
        callback(new Error('Error: Something Went Wrong!'));
    }

    getBoardInfo(value1, (err, board) => {
        if (err) {
            callback(err);
        }

        else {
            callback(null, board);
            getListInfo(board.id, (err, list) => {
                if (err) {
                    console.log(err);
                }

                else {
                    callback(null, list);
                    arr.forEach((curr) => {
                        const series = list.find((now) => now.name === curr).id;
                        getCardInfo(series, (err, card) => {
                            if (err) {
                                callback(err);
                            }

                            else {
                                callback(null, card);
                            }
                        });
                    })
                }
            })
        }
    })
}

// doAgain("Thanos", ["Mind", "Space"]);

module.exports = doAgain;