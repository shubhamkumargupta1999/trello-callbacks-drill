const getBoardInfo = require('./callback1.cjs');
const getListInfo = require('./callback2.cjs');
const getCardInfo = require('./callback3.cjs');

function doLastTime(value1, callback) {

    if (arguments.length < 1 || typeof value1 != 'string' || typeof callback != 'function') {
        callback(new Error('Error: Something Went Wrong!'));
    }

    getBoardInfo(value1, (err, board) => {
        if (err) {
            callback(err);
        }

        else {
            callback(null, board);
            getListInfo(board.id, (err, list) => {
                if (err) {
                    console.log(err);
                }

                else {
                    callback(null, list);
                    list.forEach((curr) => {
                        getCardInfo(curr.id, (err, card) => {
                            if (err) {
                                callback(err);
                            }

                            else {
                                callback(null, card);
                            }
                        })
                    });
                }
            })
        }
    })
}

module.exports = doLastTime;