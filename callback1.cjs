const fs = require('fs');

function getBoardInfo(value, callback) {
    setTimeout(() => {

        if (arguments.length < 2 || value === undefined || typeof value != 'string' || typeof callback != 'function') {
            callback(new Error('Error: Invalid Board Id.'));
        }

        fs.readFile('./boards.json', 'utf-8', (err, boardData) => {
            if (err) callback(err);
            else {
                const board = JSON.parse(boardData).find((curr) => curr.id || curr.name === value);

                callback(null, board);
            }
        });
    }, 2000);
}

module.exports = getBoardInfo;
