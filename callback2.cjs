const fs = require('fs');

function getListInfo(boardID, callback) {
    setTimeout(() => {

        if (arguments.length < 2 || boardID === undefined || typeof boardID != 'string' || typeof callback != 'function') {
            callback(new Error('Error: Invalid Board Id.'));
        }

        fs.readFile('./lists.json', 'utf-8', (err, listsData) => {
            if (err) callback(err);
            else {
                const list = JSON.parse(listsData)[boardID];

                callback(null, list);
            }
        });
    }, 2000);
}

module.exports = getListInfo;