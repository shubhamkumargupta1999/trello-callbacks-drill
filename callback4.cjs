const getBoardInfo = require('./callback1.cjs');
const getListInfo = require('./callback2.cjs');
const getCardInfo = require('./callback3.cjs');

function doEverything(value1, value2, callback) {

    if (arguments.length < 3 || typeof value1 != 'string' || typeof value2 != 'string' || typeof callback != 'function') {
        callback(new Error('Error: Something Went Wrong!'));
    }

    getBoardInfo(value1, (err, board) => {
        if (err) {
            callback(err);
        }

        else {
            callback(null, board);
            getListInfo(board.id, (err, list) => {
                if (err) {
                    callback(err);
                }

                else {
                    callback(null, list);
                    getCardInfo((list.find((curr) => curr.name === value2).id), (err, card) => {
                        if (err) {
                            callback(err);
                        }

                        else {
                            callback(null, card);
                        }
                    });
                }
            })
        }
    })
}

module.exports = doEverything;